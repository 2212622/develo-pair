//Solution
public class SumOfPositive {

    public static int sum(int[] arr){
        int sum = 0;

        // return the sum of all positive integers
        for(int i = 0; i < arr.length; i++){
            if(arr[i] > 0){
                sum += arr[i];
            }
        }
        return sum;
    }

    //Tester
    public static void main(String args[]){
        int [] arr = new int[]{1,-2,3,4,5};

        System.out.println(sum(arr));
    }
}