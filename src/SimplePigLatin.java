public class SimplePigLatin {
    public static String pigIt(String str) {
        // Write code here
        String[] split = str.split(" ");
        String answer = "";

        for(int i = 0; i < split.length; i++){
            String temp = "";
            char firstCharacter = 0;
            for(int y = 0; y < split[i].length(); y++){
                firstCharacter = split[i].charAt(0);
                if (!Character.isAlphabetic(firstCharacter)){
                    temp = String.valueOf(firstCharacter);
                } else if (y == split[i].length() - 1){
                    if (split[i].length() > 1){
                        temp = temp + split[i].charAt(y) + firstCharacter + "ay";
                    } else {
                        temp = firstCharacter + "ay";
                    }
                } else if (y > 0){
                    temp = temp + split[i].charAt(y);
                }
            }

            if(i > 0) {
                answer = answer + " " + temp;
            } else {
                answer = temp;
            }
        }
        return answer;
    }

    public static void main (String[] args){
        System.out.println(pigIt("O tempora o mores !"));
        System.out.println(pigIt("Pig latin is cool"));
        System.out.println(pigIt("This is my string"));
    }
}