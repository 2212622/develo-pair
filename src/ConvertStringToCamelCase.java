//import java.lang.StringBuilder;
public class ConvertStringToCamelCase {
    public static String toCamelCase(String s){
        String[] answer = s.split("[\\_\\-]");
        String finalAnswer = "";
        for (int i = 0; i < answer.length; i++){
            for (int j = 0; j < answer[i].length(); j++){
                if (Character.isLowerCase(answer[i].charAt(0)) && j == 0 && i != 0){
                    finalAnswer = finalAnswer + Character.toUpperCase(answer[i].charAt(0));
                } else {
                    finalAnswer = finalAnswer + answer[i].charAt(j);
                }
            }
        }
        return finalAnswer;
    }

    public static void main(String args[]){
        System.out.println(toCamelCase("the_stealth_Warrior"));
    }
}
