// Solution
public class CountingDuplicates {
    public static final int noOfChar = 150;
    public static int duplicateCount(String text) {
        int result = 0;
        int[] counter = new int[noOfChar];

        // converting all text to lowercase
        for(int i = 0; i < text.length(); ++i){
            counter[Character.toLowerCase(text.charAt(i))]++;
        }
        // number of duplicated letters
        for (int i = 0; i < noOfChar; i++) {
            if(counter[i] > 1) {
                result++;
            }
        }
        return result;
    }
// test
    public static void main(String args[]) {
        System.out.println(duplicateCount("abcde"));
        System.out.println(duplicateCount("aAbbcCde"));
        System.out.println(duplicateCount("aabb11deE"));
    }
}